export const eec_incentive_company_types = [];
export const eec_incentive_territory_types = ['AideTerritoire', 'Aide Territoire', 'AidesTerritoire', 'Aides Territoire'];

export const not_displayed_incentive_types = eec_incentive_territory_types;

export const regular = 2;
export const punctual = 1;
